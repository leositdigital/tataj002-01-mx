package com.bbva.tata.batch.step1;

import java.util.Iterator;
import java.util.List;

import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.bbva.tata.dto.account.y.AccountDTO;
import com.bbva.tata.lib.rj01.TATARJ01;

public class ReaderBDD implements ItemReader<AccountDTO>{
	private TATARJ01 tataRJ01;
	public void setTataRJ01(TATARJ01 tataRJ01) {
		this.tataRJ01 = tataRJ01;
	}
    Iterator<AccountDTO> accountIterator;
	
	@BeforeStep
    public void before() {
	 List<AccountDTO> listAccount = tataRJ01.executeGetAccount();
	 accountIterator = listAccount.iterator();    	
    }
	
	@Override
	public AccountDTO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {		
		if(accountIterator!=null && accountIterator.hasNext()) {			
			return accountIterator.next();
		}else {
			return null;
		}
	}

}
